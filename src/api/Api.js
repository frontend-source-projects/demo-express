const express = require('express')
const morgan=require('morgan')

const app = express()

//Configuraciones
app.set('port', process.env.PORT || 8080)
app.set('json spaces', 2)

//Middleware
app.use(morgan('dev'))
app.use(express.urlencoded({extended:false}))
app.use(express.json())

//Nuestro primer WS Get
app.get('/', (req, res) => {    
    res.json(
        {
            "message": "Init server!"
        }
    )
})

//Routes
app.use('/users', require('./controller/UserController'));

//Iniciando el servidor
app.listen(app.get('port'),()=>{
    console.log(`Server listening on port ${app.get('port')}`)
})